<div class="page-content fade-in-up">
    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="ibox">
                <div class="ibox-body">
                    <div class="row">
                        <div class="col-sm-4 form-group">
                          <?php $total = 0; foreach($datos['cobros'] as $cobro):
                            $total += $cobro->tbl_cobros_monto;
                          endforeach;?>
                            <label for="">Recolectado: </label>
                            <input type="text" class="form-control text-success" name="recolectado" value="<?php print(number_format($total));?>" placeholder="1000" disabled>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="">Esperado: </label>
                            <input type="text" class="form-control text-primary" name="esperado" placeholder="1000" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Cobros realizados</div>
                    <div class="ibox-tools">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="text" class="form-control hidden" name="id_empleado" value="<?php print($datos["id"]);?>" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control hidden" name="ruta_id" value="<?php print($datos['id_ruta']);?>" readonly>
                        </div>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modalCobro"><i class="fa fa-plus"> Agregar</i></button>
                      </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="example-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>CLIENTE</th>
                                    <th>MONTO</th>
                                    <th>FECHA</th>
                                    <th>ESTADO</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach($datos['cobros'] as $cobro): ?>
                                <tr>
                                    <td><?php print($cobro->tbl_cobros_id); ?></td>
                                    <td><?php print($cobro->tbl_cliente_nombre . " " . $cobro->tbl_cliente_apellido1); ?></td>
                                    <td><?php print(number_format($cobro->tbl_cobros_monto)); ?></td>
                                    <td><?php print($cobro->tbl_cobros_fecha); ?></td>
                                    <td><i class="fa fa-check text-success"></i></td>
                                </tr>
                              <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="ibox">
                <div class="ibox-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
