<?php
    defined("BASEPATH")or exit("No se permite acceso directo.");
    /**
    * clase IngresosController
    */
    class IngresosController extends Controlador{
        private $modelo;

        public function __construct(){
          $this->modelo = $this->modelo("IngresosModel");
        }

        public function setIngresos(){
          if($_SERVER["REQUEST_METHOD"] == "POST"):
            $res = $this->modelo->setIngresos(json_decode($_POST['data']));
            print(json_encode($res));
          endif;
        }
    }
