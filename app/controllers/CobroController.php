<?php
    defined("BASEPATH")or exit("No se permite el acceso directo.");
    /**
     * clase CobroController
     */
    class CobroController extends Controlador {
        private $sesion;
        private $cobroModelo;
        public $datos = array();

        public function __construct(){
            $this->sesion = new Session;
            $this->cobroModelo = $this->modelo("CobroModelo");
            if(!$this->sesion->getAll())
                header("Location: ". RUTA_URL .'/Login/index');
            $this->datos = [
                "id" => $this->sesion->get("session")->tbl_empleado_id,
                "usuario" => $this->sesion->get("session")->tbl_usuarios_usuario,
                "nombre" => $this->sesion->get("session")->tbl_empleado_nombre,
                "apellido1" => $this->sesion->get("session")->tbl_empleado_apellido1,
                "rol" => $this->sesion->get("session")->tbl_usuarios_rol,
                "id_ruta" => $this->sesion->get('session')->tbl_ruta_tbl_ruta_id
            ];
        }

        public function index(){
            $fecha = date("Y-m-d");
            $this->datos["cobros"] = $this->cobroModelo->getCobrosFecha($fecha);
            $this->vista("cobro/index", $this->datos);
        }

        public function addCobro(){
          if($_SERVER["REQUEST_METHOD"] == "POST"):
            $res = $this->cobroModelo->addCobro(json_decode($_POST['data']));
            print($res);
          endif;
        }
    }
