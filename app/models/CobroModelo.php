<?php
    defined("BASEPATH") or exit("No se permite acceso directo");
    /**
     * Clase CobroModelo
     */
    class CobroModelo {
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        public function addCobro($data){
          $sql = "INSERT INTO tbl_cobros (tbl_cobros_monto, tbl_cobros_fecha, tbl_empleado_tbl_empleado_id, tbl_cliente_tbl_cliente_id,
          tbl_ruta_tbl_ruta_id, tbl_prestamos_tbl_prestamos_id) VALUES (:monto, :fecha, :id_empleado, :id_cliente, :id_ruta, :id_prestamos);";
          $this->db->query($sql);
          $this->db->bind(':monto', $data[0]->{'monto'});
          $this->db->bind(':fecha', $data[0]->{'fecha'});
          $this->db->bind(':id_empleado', $data[0]->{'id_empleado'});
          $this->db->bind(':id_cliente', $data[0]->{'id_cliente'});
          $this->db->bind(':id_ruta', $data[0]->{'id_ruta'});
          $this->db->bind('id_prestamos', $data[0]->{'id_prestamo'});
          return $this->db->execute();
        }

        public function getCobrosFecha($fecha){
          $sql = "SELECT tbl_cobros.*, tbl_cliente.tbl_cliente_nombre, tbl_cliente.tbl_cliente_apellido1 FROM tbl_cobros INNER JOIN tbl_cliente ON tbl_cliente_id = tbl_cliente_tbl_cliente_id WHERE tbl_cobros_fecha LIKE '".$fecha." %%:%%';";
          $this->db->query($sql);
          return $this->db->registros();
        }
    }
