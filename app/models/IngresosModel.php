<?php
    defined("BASEPATH") or exit("No se permite acceso directo");

    class IngresosModel {
        private $db;

        public function __construct(){
          $this->db = new Base;
        }

        public function setIngresos($data){
          $sql = "INSERT INTO tbl_ingresos (tbl_ingresos_tipo, tbl_ingresos_titulo, tbl_ingresos_antiguedad, tbl_ingresos_direccion,
          tbl_ingresos_egresos, tbl_ingresos_ingresos, tbl_cliente_tbl_cliente_id) VALUES (:tipo, :titulo, :antiguedad, :direccion, :egresos, :ingresos, :cliente);";
          $this->db->query($sql);
          $this->db->bind(':tipo', $data[0]->{'value'});
          $this->db->bind(':titulo', $data[1]->{'value'});
          $this->db->bind(':antiguedad', $data[2]->{'value'});
          $this->db->bind(':direccion', $data[3]->{'value'});
          $this->db->bind(':ingresos', $data[4]->{'value'});
          $this->db->bind(':egresos', $data[5]->{'value'});
          $this->db->bind(':cliente', $data[6]->{'value'});
          if($this->db->execute())
            return $this->getAllIngresos();
        }

        public function getAllIngresos(){
          $sql = "SELECT tbl_ingresos_id, tbl_cliente_id, tbl_cliente_nombre, tbl_cliente_apellido1 FROM tbl_ingresos INNER JOIN tbl_cliente ON tbl_cliente_id = tbl_cliente_tbl_cliente_id;";
          $this->db->query($sql);
          return $this->db->registros();
        }
    }
